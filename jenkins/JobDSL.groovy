import javaposse.jobdsl.plugin.GlobalJobDslSecurityConfiguration
import jenkins.model.GlobalConfiguration

GlobalConfiguration.all().get(GlobalJobDslSecurityConfiguration.class).useScriptSecurity=false

folder("Whanos base images") {
    displayName("Whanos base images")
}

folder("Projects") {
    displayName("Projects")
}

freeStyleJob("Whanos base images/whanos-c") {
    steps {
        shell("docker build /images/c/ -t whanos-c -f /images/c/Dockerfile.base")
    }
}

freeStyleJob("Whanos base images/whanos-java") {
    steps {
        shell("docker build /images/java/ -t whanos-java -f /images/java/Dockerfile.base")
    }
}

freeStyleJob("Whanos base images/whanos-javascript") {
    steps {
        shell("docker build /images/javascript/ -t whanos-javascript -f /images/javascript/Dockerfile.base")
    }
}

freeStyleJob("Whanos base images/whanos-python") {
    steps {
        shell("docker build /images/python/ -t whanos-python -f /images/python/Dockerfile.base")
    }
}

freeStyleJob("Whanos base images/whanos-befunge") {
    steps {
        shell("docker build /images/befunge/ -t whanos-befunge -f /images/befunge/Dockerfile.base")
    }
}

freeStyleJob("Build all base images") {
    publishers {
        downstream('Whanos base images/whanos-c')
        downstream('Whanos base images/whanos-java')
        downstream('Whanos base images/whanos-javascript')
        downstream('Whanos base images/whanos-python')
        downstream('Whanos base images/whanos-befunge')
    }
}

freeStyleJob('link-project') {
    parameters {
        stringParam('JOBNAME', '', 'Name of the job')
        stringParam('GITURL', '', 'URL of the Git repository')
    }
    steps {
        dsl {
            text('''
                freeStyleJob("Projects/$JOBNAME") {
                    scm {
                        git {
                            remote {
                                name('origin')
                                url("$GITURL")
                            }
                        }
                    }
                    triggers {
                        scm("M/1")
                    }
                    wrappers {
                        steps {
                            shell('/find_project_language.sh')
                        }
                    }
                }
            ''')
        }
    }
}