#!/bin/bash

timestamp=$(date +%s)
value=$timestamp
image="image"

c () {
   if [ -f "Dockerfile" ]; then
        docker build -t "$value$image" .
        docker run --name="$value" "$value$image"
   else
        docker build . -t whanos-c-standalone -f /images/c/Dockerfile.standalone
        docker run --name="$value" whanos-c-standalone
    fi
}

java () {
   if [ -f "Dockerfile" ]; then
        docker build -t "$value$image" .
        docker run --name="$value" "$value$image"
        exit 0
   else
      docker build . -t whanos-java-standalone -f /images/java/Dockerfile.standalone
      docker run --name="$value" whanos-java-standalone
      exit 0
    fi
}

javascript () {
   if [ -f "Dockerfile" ]; then
        docker build -t "$value$image" .
        docker run -p 3000:3000 --name="$value" "$value$image"
   else
      docker build . -t whanos-javascript-standalone -f /images/javascript/Dockerfile.standalone
      docker run -p 3000:3000 --name="$value" whanos-javascript-standalone
    fi
}

python () {
   if [ -f "Dockerfile" ]; then
        docker build -t "$value$image" .
        docker run --name="$value" "$value$image"
   else
      docker build . -t whanos-python-standalone -f /images/python/Dockerfile.standalone
      docker run --name="$value" whanos-python-standalone
    fi
}

befunge () {
   if [ -f "Dockerfile" ]; then
        docker build -t "$value$image" .
        docker run --name="$value" "$value$image"
   else
      docker build . -t whanos-befunge-standalone -f /images/befunge/Dockerfile.standalone
      docker run --name="$value" whanos-befunge-standalone
    fi
}

if [ -f "Makefile" ]; then
    c
elif [ -f "./app/pom.xml" ]; then
    java
elif [ -f "package.json" ]; then
    javascript
elif [ -f "requirements.txt" ]; then
    python
elif [ -f "./app/main.bf" ]; then
    befunge
else
    echp "Error : could not determine the language of this project"
fi