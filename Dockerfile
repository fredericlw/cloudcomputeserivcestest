FROM jenkins/jenkins:lts

USER root

RUN cd /tmp && curl -sSL -O https://download.docker.com/linux/static/stable/x86_64/docker-20.10.9.tgz && tar -zxf docker-20.10.9.tgz && mkdir -p /usr/local/bin && mv ./docker/docker /usr/local/bin && chmod +x /usr/local/bin/docker && rm -rf /tmp/*
RUN chown -R jenkins:jenkins /var/jenkins_home/

USER jenkins

ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false
ENV CASC_JENKINS_CONFIG ./casc.yml
ENV PASSWORD whanos

COPY jenkins/plugins.txt /usr/share/jenkins/ref/plugins.txt
COPY jenkins/JobDSL.groovy ./JobDSL.groovy
COPY jenkins/casc.yml ./casc.yml
COPY jenkins/find_project_language.sh find_project_language.sh
COPY images images

RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt